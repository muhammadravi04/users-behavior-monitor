package Analysis

import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.twitter.TwitterUtils
import twitter4j.{Query, Status, TwitterFactory}

object Trends {
  //
  def getTrendingTopics(tweets:ReceiverInputDStream[Status]):Unit={

    val statuses = tweets.map(status=>status.getText)
    val words= statuses.flatMap(word=>word.split(" "))
    val hashtags=words.filter(hashtags=>hashtags.contains("#"))

    val slidingWindowSeconds=6*3600
    val hashtagKeyValues=hashtags.map(hashtags=>(hashtags,1))
    val hashtagCounts=hashtagKeyValues.reduceByKeyAndWindow((x,y)=>x+y,
      (x,y)=>x-y, Seconds(slidingWindowSeconds), Seconds(1))

    val sortedResults = hashtagCounts.transform(rdd=>rdd.sortBy(x=>x._2, false))
   sortedResults.print()
  }

  def getTrends(tweets:ReceiverInputDStream[Status]):Unit={

  }

}
