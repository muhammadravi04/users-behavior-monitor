package Configuration

import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import Conf._
import Analysis.Trends._
import org.apache.spark.SparkConf

object Main {
  def main(args: Array[String]) {
    val appName:String="UserBehavior"
    val master:String="local[*]"

    setupTwitter()
    val conf=new SparkConf().setAppName(appName).setMaster(master)
    val ssc = new StreamingContext(conf, Seconds(1))

    setupLogging()

    val tweets = TwitterUtils.createStream(ssc, None)

    getTrendingTopics(tweets)

    ssc.checkpoint("C:/checkpoint/")
    ssc.start()
    ssc.awaitTermination()
  }
}
